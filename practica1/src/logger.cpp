#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>

int main(int argc,char* argv[]){
	char buffer[250];
	mkfifo("/tmp/fifologger",0777);                //crea tuberia fifo
	int fd=open("/tmp/fifologger",O_RDONLY);       //abre tuberia en modo lectura
	while(1){
		read(fd,buffer,sizeof(buffer));        //bucle infinito de lectura de datos de la tuberia
		printf("%s\n",buffer);
	}
	close(fd);                                     //cierre del fd de la tuberia
	unlink("/tmp/fifologger");                     //eliminación de la tuberia
	return 0;

}
