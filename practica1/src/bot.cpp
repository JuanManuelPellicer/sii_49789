#include <iostream>

#include <sys/types.h>

#include <sys/stat.h>

#include <fcntl.h>

#include <unistd.h>

#include <stdlib.h>

#include <stdio.h>

#include <ctype.h>

#include "Esfera.h"

#include "Raqueta.h"

#include "DatosMemCompartida.h"



int main(int argc,char* argv[])

{

  DatosMemCompartida* pmc;                                                                      //puntero de datos a memoria compartida

  int fd=open("/tmp/bot.txt", O_RDWR);                                                          //apertura del fichero

  char *proyeccion=(char*)mmap(NULL,sizeof(*(pmc)),PROT_WRITE|PROT_READ,MAP_SHARED,file,0);     //proyeccion en memoria

  close(fd);                                                                                    //cierre del fichero

  pmc=(DatosMemCompartida*)proyeccion;                                                          //puntero de datos a la proyeccion del fichero en memoria



  while(1){                                                                                     //bucle de lógica de control

    float posY_raqueta=(pmc->raqueta1.y2+pmc->raqueta1.y1)/2;

		if(posY_raqueta<pmc->esfera.centro.y)	pmc->accion=1;	                        //raqueta sube

		else if(posY_raqueta>pmc->esfera.centro.y) pmc->accion=-1;                      //raqueta baja

		else pmc->accion=0;                                                             //raqueta  la altura de la bola

    usleep(25000);                                                                              //suspensión durante 25ms

  }

  munmap(proyeccion,sizeof(*(pmc)));                                                            //desmontado de la proyeccion de memoria

}
