#**Juan Manuel Pellicer Dobarro**
###NUMERO DE MATRICULA: **49789**
---
##Repositorio de las pr�cticas de sistemas informaticos industriales del curso 2017/2018

* Como trabajar en local:

En home:
```mkdir 49789```

```cd 49789```

```git config --global user.name JuanManuelPellicer```

```git config --global user.email JuanManuelPellicer@bitbucket.org```

```git clone https://JuanManuelPellicer@bitbucket.org/JuanManuelPellicer/sii_49789.git```

* Como crear ejecutable

```cd /home/sii/49789/sii_49789/practica1/```

```mkdir build```

```cd build```

```cmake ..```

```make```

```cd /home/sii/49789/sii_49789/practica1/build/src```

```./tenis```

* Como borrar directorio de trabajo e identidad al acabar sesion

```cd```
```rm -rf matr�cula```
```rm /home/sii/.gitconfig```
